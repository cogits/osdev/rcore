const std = @import("std");
const fs = std.fs;

pub fn main() !void {
    var arena: std.heap.ArenaAllocator = .init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    if (args.len < 3) {
        std.log.err("Usage: {s} <target> <outfile>", .{args[0]});
        return;
    }

    const target_path = args[1];
    const outfile = args[2];

    var app_list: std.ArrayList([]const u8) = .init(allocator);
    defer app_list.deinit();

    const apps = a: {
        var dir = try fs.cwd().openDir(target_path, .{ .iterate = true });
        defer dir.close();
        var iter = dir.iterate();

        while (try iter.next()) |entry| {
            if (!std.mem.endsWith(u8, entry.name, ".bin")) continue;
            const app_name = fs.path.stem(entry.name);
            try app_list.append(try allocator.dupe(u8, app_name));
        }
        break :a app_list.items;
    };

    if (apps.len == 0) {
        std.log.warn("No apps found in {s}", .{target_path});
        return;
    }

    const dirname = fs.path.dirname(outfile) orelse ".";
    const filename = fs.path.basename(outfile);

    var dir = try fs.cwd().makeOpenPath(dirname, .{});
    defer dir.close();

    const output = try dir.createFile(filename, .{ .truncate = true });
    defer output.close();

    try linkAppS(allocator, output.writer(), target_path, apps);
}

fn linkAppS(
    allocator: std.mem.Allocator,
    writer: fs.File.Writer,
    target_path: []const u8,
    apps: [][]const u8,
) !void {
    const S = struct {
        fn lessThanFn(_: void, x: []const u8, y: []const u8) bool {
            return std.mem.order(u8, x, y) == .lt;
        }
    };
    std.mem.sort([]const u8, apps, {}, S.lessThanFn);

    // addresses
    try writer.print(
        \\    .align 3
        \\    .section .data
        \\    .global _num_app
        \\_num_app:
        \\    .quad {}
        \\
    , .{apps.len});

    for (0..apps.len) |i| {
        try writer.print("    .quad app_{}_start\n", .{i});
    }
    for (0..apps.len) |i| {
        try writer.print("    .quad app_{}_name\n", .{i});
    }
    try writer.writeByte('\n');

    // binary data
    const cwd = try std.process.getCwdAlloc(allocator);
    const relative_path = try fs.path.relative(allocator, cwd, target_path);

    for (apps, 0..) |app, i| {
        const app_path = try fs.path.join(allocator, &.{ relative_path, app });
        const escaped_path = try std.mem.replaceOwned(u8, allocator, app_path, "\\", "\\\\");

        try writer.print(
            \\app_{}_start:
            \\    .incbin "{s}.bin"
            \\
        , .{ i, escaped_path });
    }
    try writer.writeByte('\n');

    // app names
    for (apps, 0..) |app, i| {
        try writer.print(
            \\app_{}_name:
            \\    .string "{s}\0"
            \\
        , .{ i, app[3..] });
    }
}
