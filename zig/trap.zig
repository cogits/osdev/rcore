const std = @import("std");
const csr = @import("csr");
const task = @import("task.zig");
const timer = @import("timer.zig");
const syscall = @import("syscall.zig").call;
const log = std.log.scoped(.trap);

extern const __alltraps: usize;

/// initialize CSR `stvec` as the entry of `__alltraps`
pub fn init() void {
    csr.raw.write(.stvec, @intFromPtr(&__alltraps));
    // enable timer interrupt
    csr.set(.sie, .{ .stie = true });
    timer.setNextTrigger(10);
}

/// handle an interrupt, exception, or system call from user space
export fn trap_handler(ctx: *Context) *Context {
    const scause = csr.read(.scause); // get trap cause
    const code = scause.getCode();

    switch (code) {
        .interrupt => |int| switch (int) {
            .@"Supervisor timer interrupt" => {
                timer.setNextTrigger(10);
                task.manager.next(.ready);
            },
            else => {
                log.err("interrupt {s}", .{@tagName(int)});
                unreachable;
            },
        },
        .exception => |except| switch (except) {
            .@"Environment call from U-mode" => {
                ctx.sepc += 4;
                ctx.x[10] = syscall(ctx.x[17], .{ ctx.x[10], ctx.x[11], ctx.x[12] });
            },

            else => {
                log.warn("{s}, stval: 0x{x}, sepc: 0x{x}!", .{
                    @tagName(except),
                    csr.raw.read(.stval),
                    csr.raw.read(.sepc),
                });
                task.manager.next(.exited);
            },
        },
    }

    return ctx;
}

pub const Context = extern struct {
    /// general regs[0..31]
    x: [32]usize = @splat(0),
    /// CSR sstatus
    sstatus: csr.sstatus,
    /// CSR sepc
    sepc: usize,

    /// init app context
    pub fn init(entry: usize, sp: usize) Context {
        var sstatus = csr.read(.sstatus);
        sstatus.spp = false; // clear SPP to 0 for user mode

        var ctx: Context = .{
            .sstatus = sstatus,
            .sepc = entry, // entry point of app
        };

        ctx.x[2] = sp; // set stack pointer to x_2 reg (sp)
        return ctx;
    }

    pub extern fn __restore(ctx: *Context) void;
};
