const std = @import("std");
const sbi = @import("sbi");
const trap = @import("trap.zig");
const rcore = @import("rcore.zig");
const loader = @import("loader.zig");
const log = std.log.scoped(.task);

/// The task manager, where all the tasks are managed.
///
/// Functions implemented on `TaskManager` deals with all task state transitions
/// and task context switching.
pub const Manager = struct {
    /// total number of tasks
    number: usize,
    /// id of current `Running` task
    current: usize = 0,
    /// task list
    tasks: [rcore.MAX_APP_NUM]Task,

    pub const Task = struct {
        status: Status = .uninit,
        ctx: Context,

        pub const Status = enum {
            uninit,
            ready,
            running,
            exited,
        };

        pub const Context = extern struct {
            /// return address of __switch ASM function
            ra: usize,
            /// kernel stack pointer of app
            sp: usize,
            /// callee saved registers:  s 0..11
            s: [12]usize = @splat(0),

            pub fn init(sp: usize) Context {
                return .{
                    .ra = @intFromPtr(&trap.Context.__restore),
                    .sp = sp,
                };
            }

            /// Switching to a different task's context happens here
            pub extern fn __switch(current: *Context, next: *const Context) void;
        };
    };

    pub fn init() Manager {
        const number: usize = loader._num_app;
        var tasks: [rcore.MAX_APP_NUM]Task = undefined;

        for (tasks[0..number], 0..) |*task, i| {
            // get app info with entry and sp and save `trap.Context` in kernel stack
            const user_stack_top: usize = @intFromPtr(&user_stacks[i]) + user_stacks[i].len;
            const kernel_stack_top: usize = @intFromPtr(&kernel_stacks[i]) + kernel_stacks[i].len;

            const app_base = loader.baseAddr(i);
            const trap_ctx: trap.Context = .init(app_base, user_stack_top);
            const kernel_sp: *trap.Context = @ptrFromInt(kernel_stack_top - @sizeOf(trap.Context));
            kernel_sp.* = trap_ctx;

            task.status = .ready;
            task.ctx = .init(@intFromPtr(kernel_sp));
        }

        return .{
            .number = number,
            .tasks = tasks,
        };
    }

    /// Run the first task in task list.
    ///
    /// Generally, the first task in task list is an idle task (we call it zero process later).
    /// But in ch3, we load apps statically, so the first task is a real app.
    pub fn first(self: *Manager) noreturn {
        var unused: Task.Context = undefined;
        var task0 = self.tasks[0];

        task0.status = .running;
        unused.__switch(&task0.ctx);
        @panic("unreachable in run_first_task!");
    }

    /// Switch current `Running` task to the task we have found,
    /// or there is no `Ready` task and we can exit with all applications completed
    pub fn next(self: *Manager, current_status: Task.Status) void {
        const current = self.current;
        self.tasks[current].status = current_status;

        // Find next `Ready` task in task list.
        const next_id = for (1..self.number + 1) |i| {
            const id = (i + current) % self.number;
            if (self.tasks[id].status == .ready) break id;
        } else {
            log.info("All applications completed!", .{});
            sbi.shutdown(.shutdown, .no_reason);
        };

        self.current = next_id;
        self.tasks[next_id].status = .running;
        self.tasks[current].ctx.__switch(&self.tasks[next_id].ctx);
        // go back to user mode
    }
};

var user_stacks: [rcore.MAX_APP_NUM][rcore.USER_STACK_SIZE]u8 align(16) = undefined;
var kernel_stacks: [rcore.MAX_APP_NUM][rcore.KERNEL_STACK_SIZE]u8 align(16) = undefined;

/// Global variable: task_manager
pub var manager: Manager = undefined;

pub fn init() void {
    manager = .init();
}
