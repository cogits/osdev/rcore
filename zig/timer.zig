const csr = @import("csr");
const sbi = @import("sbi");
const std = @import("std");

const clock_freq = 1250_0000;
const cycles_per_ms = clock_freq / std.time.ms_per_s;

/// read the `mtime` register
fn mtime() usize {
    return csr.raw.read(.time);
}

/// get current time in milliseconds
pub fn getTime() usize {
    return mtime() / cycles_per_ms;
}

/// set the timer to trigger after 'ms' milliseconds
pub fn setNextTrigger(ms: usize) void {
    const cycles = cycles_per_ms * ms;
    sbi.setTimer(mtime() + cycles);
}
