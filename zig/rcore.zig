//! Constants used in rCore

pub const APP_BASE_ADDRESS = 0x8040_0000;
pub const APP_SIZE_LIMIT = 0x10_0000;
pub const KERNEL_STACK_SIZE = 4096 * 2;
pub const USER_STACK_SIZE = 4096;
pub const MAX_APP_NUM = 4;

pub const SysCall = enum(usize) {
    write = 64,
    exit = 93,
    yield = 124,
    gettime = 169,
};
