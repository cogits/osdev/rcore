const std = @import("std");
const task = @import("task.zig");
const log = std.log.scoped(.syscall);
const timer = @import("timer.zig");
const print = @import("log.zig").print;
const SysCall = @import("rcore.zig").SysCall;

pub fn call(id: usize, args: [3]usize) usize {
    const call_id: SysCall = @enumFromInt(id);
    return switch (call_id) {
        .write => write(args[0], @ptrFromInt(args[1]), args[2]),
        .exit => exit(args[0]),
        .yield => yield(),
        .gettime => timer.getTime(),
    };
}

fn write(fd: usize, ptr: [*]const u8, len: usize) usize {
    if (fd > 2) @panic("Unsupported fd in sys_write!");
    const string = ptr[0..len];
    print("{s}", .{string});
    return len;
}

/// task exits and submit an exit code
fn exit(code: usize) noreturn {
    log.info("Application exited with code {}", .{code});
    task.manager.next(.exited);
    @panic("Unreachable in sys_exit!");
}

/// current task gives up resources for other tasks
fn yield() usize {
    task.manager.next(.ready);
    return 0;
}
