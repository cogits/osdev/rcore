const std = @import("std");
const log = std.log.scoped(.loader);
const rcore = @import("rcore.zig");

pub extern const _num_app: usize;
const app_ptrs = @extern([*]const usize, .{ .name = "_num_app" });

const AppManager = struct {
    number: usize,
    current: usize = 0,
    start: []const usize,
    names: []const [*:0]const u8,

    fn loadApp(apps: AppManager, id: usize) void {
        // clear app area
        const base: [*]u8 = @ptrFromInt(baseAddr(id));
        @memset(base[0..rcore.APP_SIZE_LIMIT], 0);
        log.info("Loading app{}: {s: <20} [0x{x}, 0x{x}) => {*}", .{
            id,
            apps.names[id],
            apps.start[id],
            apps.start[id + 1],
            base,
        });

        const source: [*]const u8 = @ptrFromInt(apps.start[id]);
        const len = apps.start[id + 1] - apps.start[id];
        std.debug.assert(len <= rcore.APP_SIZE_LIMIT);
        @memcpy(base[0..len], source[0..len]);
    }

    fn loadApps(apps: AppManager) void {
        for (0..apps.number) |i| {
            apps.loadApp(i);
        }

        // Memory fence about fetching the instruction memory
        // It is guaranteed that a subsequent instruction fetch must
        // observes all previous writes to the instruction memory.
        // Therefore, fence.i must be executed after we have loaded
        // the code of the next app into the instruction memory.
        // See also: riscv non-priv spec chapter 3, 'Zifencei' extension.
        asm volatile ("fence.i");
    }
};

/// Get base address of app i.
pub fn baseAddr(id: usize) usize {
    return rcore.APP_BASE_ADDRESS + id * rcore.APP_SIZE_LIMIT;
}

pub fn init() void {
    const app_start_ptrs: [*]const usize = app_ptrs + 1; // skip _num_app
    const app_names_ptrs: [*]const [*:0]const u8 = @ptrCast(app_ptrs + 1 + _num_app);
    const apps: AppManager = .{
        .number = _num_app,
        .start = app_start_ptrs[0 .. _num_app + 1],
        .names = app_names_ptrs[0.._num_app],
    };

    log.info("num_app: {}", .{apps.number});
    apps.loadApps();
}
