const std = @import("std");

// Although this function looks imperative, note that its job is to declaratively
// construct a build graph that will be executed by an external runner.
pub fn build(b: *std.Build) void {
    const optimize = b.standardOptimizeOption(.{});
    const riscv64 = b.resolveTargetQuery(.{
        .cpu_arch = .riscv64,
        .os_tag = .freestanding,
    });
    const trace = optimize == .Debug and
        b.option(bool, "trace", "Print stack trace in debug mode") orelse false;

    // Build user applications
    const user_builder = b.dependency("apps", .{}).builder;
    const user_install_path = user_builder.getInstallPath(.prefix, "");

    // Generate link_app.S
    const link_tool = b.addExecutable(.{
        .name = "link-tool",
        .root_source_file = b.path("zig/link.zig"),
        .target = b.graph.host,
        .optimize = optimize,
    });
    const tool_run = b.addRunArtifact(link_tool);
    tool_run.addArg(user_install_path);
    const link_app_s = tool_run.addOutputFileArg("link_app.S");
    tool_run.step.dependOn(user_builder.default_step);

    // Build rCore
    const rcore_elf = b.addExecutable(.{
        .name = "rcore.elf",
        .root_source_file = b.path("zig/main.zig"),
        .target = riscv64,
        .optimize = optimize,
        .omit_frame_pointer = false,
    });

    const exe_options = b.addOptions();
    exe_options.addOption(bool, "trace", trace);
    rcore_elf.root_module.addOptions("options", exe_options);

    if (trace) {
        const rvfsg_dep = b.lazyDependency("rvfsg", .{
            .target = riscv64,
            .optimize = optimize,
        });

        if (rvfsg_dep) |rvfsg| {
            rcore_elf.root_module.addImport("rvfsg", rvfsg.module("freestanding"));
        }
    }

    const riscv_dep = b.dependency("riscv", .{
        .target = riscv64,
        .optimize = optimize,
    });

    rcore_elf.root_module.addImport("csr", riscv_dep.module("csr"));
    rcore_elf.root_module.addImport("sbi", riscv_dep.module("sbi"));

    rcore_elf.root_module.code_model = .medium;
    const kernel_ld = if (trace) "zig/trace.ld" else "kernel.ld";
    rcore_elf.setLinkerScript(b.path(kernel_ld));
    rcore_elf.addAssemblyFile(link_app_s);
    rcore_elf.addAssemblyFile(b.path("rust/task/switch.S"));
    rcore_elf.addAssemblyFile(b.path("rust/trap/trap.S"));

    // This declares intent for the rcore.bin to be installed into the standard
    // location when the user invokes the "install" step (the default step when
    // running `zig build`).
    const rcore_bin = rcore_elf.addObjCopy(.{ .format = .bin });
    const install_rcore = b.addInstallFile(rcore_bin.getOutput(), "rcore.bin");
    b.default_step.dependOn(&install_rcore.step);
    b.default_step.name = "install rcore";

    // Run rCore in QEMU.
    const rcore_path = b.pathJoin(&.{ b.install_prefix, "rcore.bin" });
    const kernel_entry_pa = 0x80200000;
    const qemu_cmd = "qemu-system-riscv64";
    const qemu_args = [_][]const u8{
        "-machine",   "virt",
        "-bios",      "./boot/opensbi-jump-1.6.bin",
        "-device",    b.fmt("loader,file={s},addr=0x{x}", .{ rcore_path, kernel_entry_pa }),
        "-nographic",
    };

    const run_tls = b.step("run", "Run rCore in QEMU");
    const run = b.addSystemCommand(&.{qemu_cmd});
    run.addArgs(&qemu_args);

    run.step.dependOn(&install_rcore.step);
    run_tls.dependOn(&run.step);

    // run qemu with gdb server
    const qemu_tls = b.step("qemu", "Run rCore in QEMU with gdb server");
    const qemu = b.addSystemCommand(&.{qemu_cmd});
    qemu.addArgs(&qemu_args);
    qemu.addArgs(&.{ "-gdb", "tcp::26002", "-S" });

    qemu.step.dependOn(&install_rcore.step);
    qemu_tls.dependOn(&qemu.step);

    // debug with gdb
    const gdb_tls = b.step("gdb", "Debug with gdb");
    const gdb = b.addSystemCommand(&.{ "gdb-multiarch", "-q", "-x", ".gdbinit" });
    gdb.addFileArg(rcore_elf.getEmittedBin());
    gdb_tls.dependOn(&gdb.step);
}
