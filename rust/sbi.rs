//! SBI call wrappers

use core::arch::asm;

const SBI_DEBUG_CONSOLE: i32 = 0x4442434E;
const SBI_TIMER: i32 = 0x54494D45;
const SBI_SYSTEM_RESET: i32 = 0x53525354;

/// general sbi call
#[inline(always)]
fn call(eid: i32, fid: i32, arg0: usize, arg1: usize, arg2: usize) -> usize {
    let mut ret;
    unsafe {
        asm!(
            "ecall",
            inlateout("x10") arg0 => _, // ignore errors
            inlateout("x11") arg1 => ret,
            in("x12") arg2,
            in("x16") fid,
            in("x17") eid,
        );
    }
    ret
}

/// use sbi call to putchar in console (qemu uart handler)
#[allow(dead_code)]
pub fn console_putchar(c: usize) {
    call(SBI_DEBUG_CONSOLE, 2, c, 0, 0);
}

/// use sbi call to putstring in console (qemu uart handler)
pub fn console_putstring(s: &str) {
    let bytes = s.as_bytes();
    call(
        SBI_DEBUG_CONSOLE,
        0,
        bytes.len(),
        bytes.as_ptr() as usize,
        0,
    );
}

/// use sbi call to set timer
pub fn set_timer(timer: usize) {
    call(SBI_TIMER, 0, timer, 0, 0);
}

/// use sbi call to shutdown the kernel
pub fn shutdown() -> ! {
    call(SBI_SYSTEM_RESET, 0, 0, 0, 0);
    unreachable!();
}
