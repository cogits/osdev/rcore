const std = @import("std");
const ld = @import("loopdir");
const fs = std.fs;
const Build = std.Build;
const String = []const u8;

const apps_path = "apps/ch3b";
const id: ld.Config.Id = @enumFromInt(1012);
const base_address = 0x8040_0000;
const step = 0x10_0000;

pub fn build(b: *Build) !void {
    const target = b.resolveTargetQuery(.{
        .cpu_arch = .riscv64,
        .os_tag = .freestanding,
    });

    const optimize = b.standardOptimizeOption(.{});
    const os_module = b.createModule(.{
        .root_source_file = b.path("os.zig"),
    });

    const buildFn = ld.recursive(buildApp, ld.mempty);
    const build_dir = ld.loop(buildFn, b, apps_path, .{
        .target = target,
        .optimize = optimize,
        .tls_depth = 2,
        .datalist = &.{
            .{ id, os_module },
        },
    }).?;

    b.default_step.dependOn(build_dir);
    b.default_step.name = "install rcore user applications";
}

fn buildApp(owner: *Build, opt: ld.Options) ?*Build.Step {
    if (opt.kind != .file) return null;
    if (!std.mem.eql(u8, fs.path.extension(opt.name), ".zig")) return null;

    const s = struct {
        var index: usize = 0;
    };
    defer s.index += 1;

    const file_path = owner.pathJoin(&.{ opt.sub_path, opt.name });
    const output_name = getOutputName(owner.allocator, s.index, file_path) catch unreachable;

    const exe = owner.addExecutable(.{
        .name = output_name,
        .root_source_file = owner.path(file_path),
        .target = opt.build.target,
        .optimize = opt.build.optimize,
    });

    exe.image_base = base_address + step * s.index;
    exe.entry = .{ .symbol_name = "_start" };
    exe.setLinkerScript(owner.path("./linker.ld"));
    exe.link_gc_sections = true;

    const os_module: *Build.Module = @constCast(@alignCast(@ptrCast(opt.build.getData(id))));
    exe.root_module.addImport("os", os_module);
    exe.root_module.code_model = .medium;

    const bin = exe.addObjCopy(.{ .format = .bin });
    const install_bin = owner.addInstallFile(bin.getOutput(), owner.fmt("{s}.bin", .{output_name}));
    return &install_bin.step;
}

fn getOutputName(allocator: std.mem.Allocator, index: usize, file_path: []const u8) ![]const u8 {
    const start_index = std.mem.indexOf(u8, file_path, "apps").? + "apps".len + 1;
    const end_index = std.mem.lastIndexOfScalar(u8, file_path, '.').?;

    const buffer = try std.fmt.allocPrint(allocator, "{d:0>2}_{s}", .{ index, file_path[start_index..end_index] });
    std.mem.replaceScalar(u8, buffer, fs.path.sep_posix, '_');
    std.mem.replaceScalar(u8, buffer, fs.path.sep_windows, '_');
    return buffer;
}

test getOutputName {
    const allocator = std.testing.allocator;
    {
        const path = "./apps/ch2b/bad_address.zig";
        const output_name = try getOutputName(allocator, 0, path);
        defer allocator.free(output_name);
        try std.testing.expectEqualStrings("00_ch2b_bad_address", output_name);
    }
    {
        const path = "apps\\ch2b\\bad_address.zig";
        const output_name = try getOutputName(allocator, 1, path);
        defer allocator.free(output_name);
        try std.testing.expectEqualStrings("01_ch2b_bad_address", output_name);
    }
}
