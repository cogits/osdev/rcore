const std = @import("std");

const SysCall = enum(usize) {
    dup = 24,
    unlinkat = 35,
    linkat = 37,
    openat = 56,
    close = 57,
    pipe = 59,
    read = 63,
    write = 64,
    fstat = 80,
    exit = 93,

    sleep = 101,
    yield = 124,
    kill = 129,
    sigaction = 134,
    sigprocmask = 135,
    sigreturn = 139,
    set_priority = 140,
    gettime = 169,
    getpid = 172,
    gettid = 178,

    sbrk = 214,
    munmap = 215,
    fork = 220,
    exec = 221,
    mmap = 222,
    waitpid = 260,

    spawn = 400,
    mail_read = 401,
    mail_write = 402,
    task_info = 410,
    thread_create = 460,
    waittid = 462,
    mutex_create = 463,
    mutex_lock = 464,
    mutex_unlock = 466,
    semaphore_create = 467,
    semaphore_up = 468,
    enable_deadlock_detect = 469,
    semaphore_down = 470,
    condvar_create = 471,
    condvar_signal = 472,
    condvar_wait = 473,
};

comptime {
    for (std.meta.fields(SysCall)) |syscall| {
        asm (entry(syscall));
    }
}

fn entry(comptime syscall: std.builtin.Type.EnumField) []const u8 {
    return std.fmt.comptimePrint(
        \\.section .text.syscall.{0s}
        \\.global {0s}
        \\{0s}:
        \\ li a7, {1d}
        \\ ecall
        \\ ret
        \\
    , .{ syscall.name, syscall.value });
}

pub extern fn fork() isize;
pub extern fn getpid() pid_t;
pub extern fn exit(code: i32) noreturn;
pub extern fn kill(pid: pid_t, signal: i32) isize;
pub extern fn exec(path: [*:0]const u8, args: [*:null]const ?[*:0]const u8) isize;
pub extern fn yield() void;

pub extern fn openat(dirfd: fd_t, path: [*:0]const u8, flags: u32, mode: u32) isize;
pub extern fn close(fd: fd_t) isize;
pub extern fn read(fd: fd_t, buffer: [*]u8, nbyte: usize) isize;
pub extern fn write(fd: fd_t, buffer: [*]const u8, nbyte: usize) isize;
pub extern fn dup(fd: fd_t) isize;
pub extern fn pipe(fds: *[2]fd_t) isize;

pub extern fn gettime() isize;
pub extern fn sleep(ms: usize) isize;
pub extern fn sbrk(size: i32) [*]u8;

pub const pid_t = isize;
pub const fd_t = isize;

pub const E = enum(16) {
    SUCCESS = 0,
    FAILURE = 1,
};

/// Get the errno from a syscall return value, or 0 for no error.
pub fn getErrno(r: usize) E {
    const signed_r = @as(isize, @bitCast(r));
    const int = if (signed_r > -4096 and signed_r < 0) -signed_r else 0;
    return @as(E, @enumFromInt(int));
}
