const std = @import("std");
pub const os = @import("os");
comptime {
    _ = os;
}

pub fn main() noreturn {
    os.print("Try to access privileged CSR in U Mode\n", .{});
    os.print("Kernel should kill this application!\n", .{});

    const value = asm volatile ("csrr %[ret], sstatus"
        : [ret] "=r" (-> usize),
    );

    var buffer: [100]u8 = undefined;
    const msg = std.fmt.bufPrint(&buffer,
        \\\(-_-) I get sstatus: 0x{x}
        \\\FAIL: T.T
    , .{value}) catch unreachable;
    @panic(msg);
}
