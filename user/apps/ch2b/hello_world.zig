pub const os = @import("os");
comptime {
    _ = os;
}

pub fn main() void {
    os.print("Hello, world!\n", .{});
}
