pub const os = @import("os");
comptime {
    _ = os;
}

pub fn main() noreturn {
    os.print("Try to execute privileged instruction in U Mode\n", .{});
    os.print("Kernel should kill this application!\n", .{});
    asm volatile ("sret");
    unreachable;
}
