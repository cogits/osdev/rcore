pub const os = @import("os");
comptime {
    _ = os;
}

const width: usize = 10;
const height: usize = 5;

pub fn main() void {
    for (0..height) |i| {
        for (0..width) |_| {
            os.print("A", .{});
        }
        os.print(" [{}/{}]\n", .{ i + 1, height });
        os.system.yield();
    }
    os.print("Test write_a OK!\n", .{});
}
