pub const os = @import("os");
comptime {
    _ = os;
}

const width: usize = 10;
const height: usize = 3;

pub fn main() void {
    for (0..height) |i| {
        for (0..width) |_| {
            os.print("C", .{});
        }
        os.print(" [{}/{}]\n", .{ i + 1, height });
        os.system.yield();
    }
    os.print("Test write_c OK!\n", .{});
}
