pub const os = @import("os");
comptime {
    _ = os;
}

const len: usize = 100;
const p: u32 = 3;
const m: u64 = 998244353;
const iter: usize = 20_0000;

pub fn main() void {
    var s: [len]u64 = @splat(0);
    var cur: usize = 0;
    s[cur] = 1;

    for (1..iter + 1) |i| {
        const next = if (cur + 1 == len) 0 else cur + 1;
        s[next] = s[cur] * p % m;
        cur = next;
        if (i % 10000 == 0) {
            os.print("power_3 [{}/{}]\n", .{ i, iter });
        }
    }

    os.print("{}^{}={}(MOD {})\n", .{ p, iter, s[cur], m });
    os.print("Test power_3 OK!\n", .{});
}
