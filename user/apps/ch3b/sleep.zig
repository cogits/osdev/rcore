pub const os = @import("os");
comptime {
    _ = os;
}

const width: usize = 10;
const height: usize = 3;

pub fn main() void {
    const current = os.system.gettime();
    const wait_for = current + 3000;
    while (os.system.gettime() < wait_for) {
        os.system.yield();
    }
    os.print("Test sleep OK!\n", .{});
}
